'use strict';

// Configuration des routes
app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
		.when('/', { templateUrl: 'views/graphe-view.html' })
		.otherwise({ redirectTo: '/' });
}]);