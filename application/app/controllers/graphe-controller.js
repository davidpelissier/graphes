'use strict';

app.controller('GrapheController', function ($rootScope, $scope, $log) {

	$scope.dessin  = new sigma('container');
	$scope.graphe  = null;
	$scope.pasApas = true;
	$scope.matrice = "0 1 1 0\n1 0 1 0\n1 1 0 1\n0 0 1 0";
	$scope.type    = 'larg';

	$scope.dessin.settings({
		labelHoverShadow: false
	});

	$scope.initialiser = function() {
		$scope.graphe = new Graphe($scope.matrice);
		$scope.tracer();
	}

	$scope.tracer = function() {
		$scope.dessin.graph.clear();

		// Création des sommets
		for( var i = 0; i < $scope.graphe.sommets.length; i++) {
			$scope.dessin.graph.addNode({
				id   : $scope.graphe.sommets[i].id,
				label: $scope.graphe.sommets[i].id,
				x    : $scope.graphe.sommets[i].x,
				y    : $scope.graphe.sommets[i].y,
				color: $scope.graphe.sommets[i].couleur,
				size : 1,
			});
		}

		// Création des relations
		for( var i = 0; i < $scope.graphe.relations.length; i++) {
			$scope.dessin.graph.addEdge({
				id: 'e'+ i,
				source: $scope.graphe.relations[i][0],
				target: $scope.graphe.relations[i][1]
			});
		}

		$scope.dessin.refresh();
	}


	$scope.parcoursProfondeur = function() {
		$scope.tracer();

		var aVisiter      = $scope.graphe.sommets;
		var pileMarques   = [];
		$scope.preVisite  = [];
		$scope.postVisite = [];

		// 1. Tous les sommets sont non marqués
		// 2. Tant qu'il existe un sommet S non marqué
		for (var i = 0; i < aVisiter.length; i++) {
			var sommetS = aVisiter[i];
			$log.debug('\n Sommet S : '+ sommetS.id);
			
			if (pileMarques.indexOf(sommetS) < 0 && sommetS.marque == 0) {

				// 3. Marquer le sommet S ouvert et l'ajouter au sommet de la pile
				sommetS.marque = 1;
				sommetS.couleur = '#77E054';
				pileMarques.unshift(sommetS);
				$scope.preVisite.push(sommetS.id);
				if ($scope.pasApas) {
					alert('Visite du sommet : '+ sommetS.id);		
				}
				$scope.tracer();
				// 4. Tant que la file n'est pas vide
				while (pileMarques.length > 0) {
					
					// 5. S'il existe un sommet Y non marqué adjacent à X situé au sommet de la pile
					var sommetX = pileMarques[0];
					var cond = false;
					$log.debug('\n Sommet X : '+ sommetX.id);

					for (var j = 0; j < sommetX.successeurs.length; j++) {
						var sommetY = $scope.graphe.getSommet(sommetX.successeurs[j]);
						$log.debug('\n Sommet Y : '+ sommetY.id);	
						if (sommetY.marque == 0) {
							// 6. Ouvrir Y et insérer Y dans la pile
							sommetY.marque = 1;
							sommetY.couleur = '#77E054';
							if ($scope.pasApas) {
								alert('Visite du successeur : '+ sommetY.id);
							}
							$scope.tracer();
							pileMarques.unshift(sommetY);
							$scope.preVisite.push(sommetY.id);
							cond = true;
						}
					}
					if (!cond) {
						// 7. Fermer le sommet X et le supprimer de la pile
						sommetX.marque = 2;
						sommetX.couleur = '#2BBAF0';
						pileMarques.shift(sommetX);
						$scope.postVisite.push(sommetX.id);
						if ($scope.pasApas) {
							alert('Fermeture du sommet : '+ sommetX.id);
						}
						$scope.tracer();
					}
				}
			}
		}		
	}


	$scope.parcoursLargeur = function() {
		$scope.tracer();

		var fileMarques = [];
		$scope.preVisite   = [];
		$scope.postVisite  = [];

		// 1. Tous les sommets sont non marqués
		// 2. Tant qu'il existe un sommet S non marqué
		for (var i = 0; i < $scope.graphe.sommets.length; i++) {
			var sommetS = $scope.graphe.sommets[i];
			$log.debug('\n Sommet S : '+ sommetS.id);		
			if (fileMarques.indexOf(sommetS) < 0 && sommetS.marque == 0) {

				// 3. Marquer le sommet S ouvert et l'insérer dans la file
				sommetS.marque = 1;
				sommetS.couleur = '#77E054';
				if ($scope.pasApas) {
					alert('Visite du sommet : '+ sommetS.id);
				}
				$scope.tracer();
				fileMarques.push(sommetS);
				$scope.preVisite.push(sommetS.id);

				// 4. Tant que la file n'est pas vide
				while (fileMarques.length > 0) {

					// 5. Supprimer le sommet X en tête de la file
					var sommetX = fileMarques.shift();
					$log.debug('\n Sommet X : '+ sommetX.id);			

					// 6. Ouvrir et insérer en fin de file tous les sommets Y non marqués adjacents à X
					for (var j = 0; j < sommetX.successeurs.length; j++) {
						var sommetY = $scope.graphe.getSommet(sommetX.successeurs[j]);
						$log.debug('\n Sommet Y : '+ sommetY.id);	
						if (fileMarques.indexOf(sommetY) < 0 && sommetY.marque == 0) {
							sommetY.marque = 1;
							sommetY.couleur = '#77E054';
							if ($scope.pasApas) {
								alert('Visite du sommet adjacent : '+ sommetY.id);
							}
							$scope.tracer();
							fileMarques.push(sommetY);
							$scope.preVisite.push(sommetY.id);
						}
					}

					// 7. Fermer le sommet X
					sommetX.marque = 2;
					sommetX.couleur = '#2BBAF0';
					$scope.postVisite.push(sommetX.id);	
					if ($scope.pasApas) {
						alert('Fermeture du sommet : '+ sommetX.id);
					}
					$scope.tracer();			
				}
			}
		}
	}
});