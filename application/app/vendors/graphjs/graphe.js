function Graphe(matrice) {
	this.matrice = matrice;
	this.sommets = [];
	this.relations = [];

	var lignes    = this.matrice.split('\n');
	var relations = [];
	// Création des sommets
	for(idSommet = 0; idSommet < lignes.length; idSommet++) {
		// On extrait les relations du sommet courant avec les autres sommets
		relations[idSommet] = lignes[idSommet].split(' ');
		successeurs = [];

		// Création des relations du sommet courant
		for(idColonne = 0; idColonne < lignes.length; idColonne++) {
			if (relations[idSommet][idColonne] == "1") {
				this.relations.push([String.fromCharCode(idSommet + 65), String.fromCharCode(idColonne + 65)]);
				successeurs.push(String.fromCharCode(idColonne + 65));
			}
		}

		// On ajoute le sommet au graphe
		var courant = new Sommet(
			String.fromCharCode(idSommet + 65), 	// 65 correspond au code lettre du A
			Math.random()*20%10,
			Math.random()*20%10,
			successeurs
			);
		this.sommets.push(courant);
	}
}

Graphe.prototype.getSommet = function(id) {
	index = this.sommets.map(function(e) { return e.id; }).indexOf(id);
	return this.sommets[index];
};